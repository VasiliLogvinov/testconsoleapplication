package by.scada;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 * Hello world!
 */
public class App {

    private static final Pattern PATTERN = Pattern.compile(".*Sample Text.*");

    public static void main(String[] args) throws InterruptedException {
        System.out.println("start main");

        List<String> stringList = Arrays.asList("1","2","3");

        Map<String, List<String>> stringListMap2 = new HashMap<String, List<String>>(){{
            put("1", Arrays.asList("11", "12", "13"));
            put("2", Arrays.asList("21", "22", "23"));
            put("3", Arrays.asList("31", "32", "33"));
        }};

        Map<String, List<String>> stringListMap3 = new HashMap<String, List<String>>(){{
            put("11", Arrays.asList("111", "112", "113"));
            put("12", Arrays.asList("121", "122", "123"));
            put("13", Arrays.asList("131", "132", "133"));
            put("21", Arrays.asList("211", "212", "213"));
            put("22", Arrays.asList("221", "222", "223"));
            put("23", Arrays.asList("231", "232", "233"));
            put("31", Arrays.asList("311", "312", "313"));
            put("32", Arrays.asList("321", "322", "323"));
            put("33", Arrays.asList("331", "332", "333"));
        }};

        Map<String, List<String>> stringListMap4 = new HashMap<String, List<String>>(){{
            put("111", Arrays.asList("1111", "1112", "1113", "1114", "1115", "1116", "1117", "1118", "1119"));
            put("112", Arrays.asList("1121", "1122", "1123", "1124", "1125", "1126", "1127", "1128", "1129"));
            put("113", Arrays.asList("1131", "1132", "1133", "1134", "1135", "1136", "1137", "1138", "1139"));
            put("121", Arrays.asList("1211", "1212", "1213", "1214", "1215", "1216", "1217", "1218", "1219"));
            put("122", Arrays.asList("1221", "1222", "1223", "1224", "1225", "1226", "1227", "1228", "1229"));
            put("123", Arrays.asList("1231", "1232", "1233", "1234", "1235", "1236", "1237", "1238", "1239"));
            put("131", Arrays.asList("1311", "1312", "1313", "1314", "1315", "1316", "1317", "1318", "1319"));
            put("132", Arrays.asList("1321", "1322", "1323", "1324", "1325", "1326", "1327", "1328", "1329"));
            put("133", Arrays.asList("1331", "1332", "1333", "1334", "1335", "1336", "1337", "1338", "1339"));
            put("211", Arrays.asList("2111", "2112", "2113", "2114", "2115", "2116", "2117", "2118", "2119"));
            put("212", Arrays.asList("2121", "2122", "2123", "2124", "2125", "2126", "2127", "2128", "2129"));
            put("213", Arrays.asList("2131", "2132", "2133", "2134", "2135", "2136", "2137", "2138", "2139"));
            put("221", Arrays.asList("2211", "2212", "2213", "2214", "2215", "2216", "2217", "2218", "2219"));
            put("222", Arrays.asList("2221", "2222", "2223", "2224", "2225", "2226", "2227", "2228", "2229"));
            put("223", Arrays.asList("2231", "2232", "2233", "2234", "2235", "2236", "2237", "2238", "2239"));
            put("231", Arrays.asList("2311", "2312", "2313", "2314", "2315", "2316", "2317", "2318", "2319"));
            put("232", Arrays.asList("2321", "2322", "2323", "2324", "2325", "2326", "2327", "2328", "2329"));
            put("233", Arrays.asList("2331", "2332", "2333", "2334", "2335", "2336", "2337", "2338", "2339"));
            put("311", Arrays.asList("3111", "3112", "3113", "3114", "3115", "3116", "3117", "3118", "3119"));
            put("312", Arrays.asList("3121", "3122", "3123", "3124", "3125", "3126", "3127", "3128", "3129"));
            put("313", Arrays.asList("3131", "3132", "3133", "3134", "3135", "3136", "3137", "3138", "3139"));
            put("321", Arrays.asList("3211", "3212", "3213", "3214", "3215", "3216", "3217", "3218", "3219"));
            put("322", Arrays.asList("3221", "3222", "3223", "3224", "3225", "3226", "3227", "3228", "3229"));
            put("323", Arrays.asList("3231", "3232", "3233", "3234", "3235", "3236", "3237", "3238", "3239"));
            put("331", Arrays.asList("3311", "3312", "3313", "3314", "3315", "3316", "3317", "3318", "3319"));
            put("332", Arrays.asList("3321", "3322", "3323", "3324", "3325", "3326", "3327", "3328", "3329"));
            put("333", Arrays.asList("3331", "3332", "3333", "3334", "3335", "3336", "3337", "3338", "3339"));
        }};


        String str =
                stringList.stream()
                        .flatMap(s -> {
                            System.out.println(s);
                            return stringListMap2.get(s).stream();
                        })
                        .flatMap(s -> {
                            System.out.println(s);
                            return stringListMap3.get(s).stream();
                        })
                        .flatMap(s -> {
                            System.out.println(s);
                            return stringListMap4.get(s).stream();
                        })
                        .findAny()
                        .get();

        System.out.println("RESULT1 = " + str);

        Stream<String> stream1 = stringList.stream();
        Stream<String> stream2 = stream1.flatMap(s -> {
            System.out.println(s);
            return stringListMap2.get(s).stream();
        });
        Stream<String> stream3 = stream2.flatMap(s -> {
            System.out.println(s);
            return stringListMap3.get(s).stream();
        });
        Stream<String> stream4 = stream3.flatMap(s -> {
            System.out.println(s);
            return stringListMap4.get(s).stream();
        });

        final String[] str2 = new String[1];

        try {
            stream4.forEach(s -> {
                str2[0] = s;
                throw new RuntimeException("1");
            });
        } catch (RuntimeException x) {}

        System.out.println("RESULT2 = " + str2[0]);

        System.out.println("end main");
    }

}
